from typing import List


class FieldMapper:
    def __init__(self, name: str, position: int, required: bool = False, data_type: object = str):
        self.name = name
        self.position = int(position)
        self.required = bool(required)
        self.data_type = eval(data_type) if isinstance(data_type, str) else data_type

    def __str__(self):
        return self.name


class FieldItem(FieldMapper):
    def __init__(self, index: int, name: str, value: str, position: int, has_error: bool):
        self.index = index
        self.name = name
        self.value = value
        self.position = position
        self.has_error = has_error


class FileMapper:
    def __init__(self, start_line=1, end_line=1, field_mapper=None):
        self.start_line = start_line - 1 if start_line > 0 else 0
        self.end_line = end_line - 1 if end_line > 0 else 0

        self.mappers = self.initialize(field_mapper)

    def is_valid(self, field_mapper):
        return field_mapper is not None and isinstance(field_mapper, FieldMapper)

    def initialize(self, field_mapper):
        items = []
        if self.is_valid(field_mapper):
            items = [field_mapper]
        return items

    def append(self, field_mapper):
        items = self.mappers
        if self.is_valid(field_mapper):
            items.append(field_mapper)
        return items

    def read_file(self, mappers: List[FieldMapper], file: str) -> list:
        result = []
        f = open(file, "r")
        lines = f.readlines()
        for index, chars in enumerate(lines):
            if self.start_line <= index <= self.end_line:
                field_item_row = self.map_field(mappers, chars)
                result.append(field_item_row)
        return result

    def map_field(self, mappers: List[FieldMapper], chars: str) -> list:
        default_end = len(chars) - 1
        result = []
        for index, item in enumerate(mappers):
            start = item.position - 1
            next_item = mappers[index + 1] if (len(mappers) > index + 1) else None
            end = next_item.position - 1 if next_item else default_end
            value = chars[start:end]
            has_error = self.check_error(value, item.required, item.data_type)
            # field_item = FieldItem(index, item.name, value, item.position, has_error)
            field_item = {'index': index,
                          'name': item.name,
                          'position': item.position,
                          'data_type': str(item.data_type),
                          'value': value,
                          'has_error': has_error}
            result.append(field_item)
        return result

    def check_error(self, value: str, required: bool, data_type: object):
        try:
            parsed_value = data_type(value)
        except ValueError:
            return True
        return (required and len(value) < 1) or not isinstance(parsed_value, data_type)


def console_input():
    start_line = int(input("Enter start line number : "))
    end_line = int(input("Enter end line number : "))
    file_mapper = FileMapper(start_line, end_line)

    num = 1
    while num == 1:
        print("Enter the fields")
        name = str(input("Enter the field name : "))
        position = int(input("Enter the field position : "))
        required = bool(input("Enter True or False if required : "))
        data_type = eval(input("Enter data type (int or str) : "))

        field_mapper = FieldMapper(name, position, required, data_type)
        mappers = file_mapper.append(field_mapper)
        num = int(input("Enter 1 for continue, other for cancel : "))

    file_mapper.read_file(mappers, "batch.txt")


def unit_test():
    mappers = [
        FieldMapper("name", 1, True, str),
        FieldMapper("salary", 5, True, int),
        FieldMapper("company", 9, True, str),
        FieldMapper("country", 13, True, str),
        FieldMapper("year", 17, False, int),
        FieldMapper("gender", 21, True, int),
        FieldMapper("comment", 25, False, str),
    ]

    file_mapper = FileMapper(1, 3)
    items = file_mapper.read_file(mappers, "batch.txt")
    print(len(items))


def main():
    unit_test()

# if __name__=="__main__":
#     main()
