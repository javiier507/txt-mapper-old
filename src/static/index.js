new Vue({
    el: '#app',
    data: {
        start_line: 1,
        end_line: 1,
        file: null,
        fields: [
        ],
        name: '',
        position: 1,
        data_type: 'str',
        required: false,
        results: []
    },
    methods: {
        add: function () {
            let { name, position, data_type, required } = this;

            let newField = {
                name,
                position,
                data_type,
                required
            };

            this.fields.push(newField);
            this.name = '';
        },
        remove: function (name) {
            this.fields = this.fields.filter(item => item.name !== name);
        },
        handleUpload: function() {
            this.file = this.$refs.files.files[0];
        },
        send: async function () {
            let formData = new FormData();

            formData.append('start_line', this.start_line);
            formData.append('end_line', this.end_line);
            formData.append('file', this.file);
            formData.append('fields', JSON.stringify(this.fields));

            try {
                let response = await fetch('/upload', {
                    method: 'POST',
                    body: formData
                });
                let results = await response.json();
                this.results = results;

            }catch (e) {

            }
        }
    }
});