import os
from flask import Flask, render_template, request, jsonify
from werkzeug.utils import secure_filename
import json

from src.mapper import FieldMapper, FileMapper

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def upload():
    start_line = int(request.form['start_line'])
    end_line = int(request.form['end_line'])

    file = request.files['file']
    filename = secure_filename(file.filename)
    path = os.path.join('media', filename)
    file.save(path)

    fields = request.form['fields']
    fields_data = json.loads(fields) if fields else []
    mappers = list(map(lambda x: FieldMapper(**x), fields_data))

    file_mapper = FileMapper(start_line, end_line)
    results = file_mapper.read_file(mappers, path)[0]

    return jsonify(results)


@app.template_filter()
def vue(item):
    return "{{ " + item + " }}"


app.run()


if __name__ == '__main__':
    app.run()
